# Useful commands
#### Run database
``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up -d database
```

### Run locally  
- Run the database.
- Run the StocksApplication main class. 
#### swagger
After running locally swagger documentation is in http://localhost:8080/swagger-ui.html

### build docker 
```
docker build -f cicd/build/Dockerfile .
```

### run with docker compose
Create BRANCH environment variable in `.env` file.
BRANCH should correspond to the tag of the image built in the repository.
Then run next command:
``` 
docker-compose -f cicd/docker-compose.yml --project-directory . up --build -d
``` 

> *NOTE:* run  these commands from the project directory
