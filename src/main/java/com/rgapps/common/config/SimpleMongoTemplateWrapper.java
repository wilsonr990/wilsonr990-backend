package com.rgapps.common.config;

import org.springframework.data.mongodb.core.MongoTemplate;

public interface SimpleMongoTemplateWrapper {
    MongoTemplate getMongoTemplate();
}
