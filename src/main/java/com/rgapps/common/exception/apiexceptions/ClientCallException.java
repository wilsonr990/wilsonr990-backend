package com.rgapps.common.exception.apiexceptions;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.Objects;

@Slf4j
public class ClientCallException extends ApiException {
    public ClientCallException(WebClientException e, String url) {
        super(new ApiErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
                ClientCallException.class.getSimpleName(), Objects.requireNonNull(e.getMessage())), e);

        log.warn(String.format("Client Call Exception: (url: %s)", url), e);
    }

    public ClientCallException(WebClientException e, String url, String payload) {
        super(new ApiErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
                ClientCallException.class.getSimpleName(), Objects.requireNonNull(e.getMessage())), e);

        log.warn(String.format("Client Call Exception: (url: %s, payload: %s)", url, payload), e);
    }
}
