package com.rgapps.common.exception.apiexceptions;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.ApiException;
import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.TreeMap;

public class NotFoundException extends ApiException {
    public NotFoundException(String searchedObjectName, Map<String, String> searchParameters) {
        super(new ApiErrorResponse(HttpStatus.NOT_FOUND,
                NotFoundException.class.getSimpleName(),
                buildMessage(searchedObjectName, searchParameters)));
    }

    public NotFoundException(String searchedObject, String paramName, String paramValue) {
        super(new ApiErrorResponse(HttpStatus.NOT_FOUND,
                NotFoundException.class.getSimpleName(),
                buildMessage(searchedObject, Map.of(paramName, paramValue))));
    }

    private static String buildMessage(String searchedObject, Map<String, String> expected) {
        Map<String, String> sorted = new TreeMap<>(expected);
        return String.format("Not found '%s' (%s)", searchedObject, sorted);
    }
}
