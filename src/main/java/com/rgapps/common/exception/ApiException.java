package com.rgapps.common.exception;

public class ApiException extends RuntimeException {
    public ApiErrorResponse errorResponse;

    public ApiException(ApiErrorResponse response) {
        super(response.getMessage());
        this.errorResponse = response;
    }

    public ApiException(ApiErrorResponse response, Throwable e) {
        super(response.getMessage(), e);
        this.errorResponse = response;
    }
}

