package com.rgapps.auth.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@Document(collection = "role")
public class Role {
    public enum RoleType {
        ROLE_USER, ROLE_MODERATOR, ROLE_ADMIN
    }

    @Id
    protected UUID id;
    @EqualsAndHashCode.Exclude
    protected LocalDateTime createdDate;
    @EqualsAndHashCode.Exclude
    protected LocalDateTime updatedDate;

    protected RoleType type;

    public Role(RoleType type) {
        this.type = type;

        id = UUID.randomUUID();
        createdDate = LocalDateTime.now();
        updatedDate = createdDate;
    }
}
