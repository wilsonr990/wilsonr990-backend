package com.rgapps.auth.repository;

import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.Role.RoleType;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface RoleRepository extends ReactiveMongoRepository<Role, UUID> {
    Mono<Role> findByType(RoleType type);
}
