package com.rgapps.auth.controller;

import com.rgapps.auth.controller.resource.AuthResponse;
import com.rgapps.auth.controller.resource.LoginResource;
import com.rgapps.auth.controller.resource.SignupResource;
import com.rgapps.auth.controller.resource.UserView;
import com.rgapps.auth.service.AuthenticationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@Validated
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authService;

    @PostMapping("/signin")
    public Mono<AuthResponse> authenticateUser(@Valid @RequestBody LoginResource resource) {
        return authService.authenticateUser(resource);
    }

    @PostMapping("/signup")
    public Mono<UserView> registerUser(@Valid @RequestBody SignupResource resource) {
        return authService.registerUser(resource)
                .map(UserView::fromUser);
    }

    @GetMapping("/users")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('ADMIN')")
    public Flux<UserView> getUsers() {
        return authService.getUsers()
                .map(UserView::fromUser);
    }

    @GetMapping("/me")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("isAuthenticated()")
    public Mono<UserView> me() {
        return authService.getCurrentUser()
                .map(UserView::fromUser);
    }
}
