package com.rgapps.auth.controller.resource;

import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import lombok.Data;

import java.util.Set;

@Data
public class UserView {
    private final String username;
    private final Set<Role> roles;

    public static UserView fromUser(User user) {
        return new UserView(user.getUsername(), user.getRoles());
    }
}
