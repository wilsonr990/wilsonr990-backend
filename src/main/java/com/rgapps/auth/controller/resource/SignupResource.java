package com.rgapps.auth.controller.resource;

import com.rgapps.auth.model.Role;
import com.rgapps.common.validator.CollectionWithEnums;
import lombok.Data;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SignupResource {
    @NotEmpty
    @Size(min = 3, max = 20)
    private String username;
    @NotEmpty
    @Size(min = 6, max = 40)
    private String password;
    @NotEmpty
    @CollectionWithEnums(Role.RoleType.class)
    private List<String> roles;
}
