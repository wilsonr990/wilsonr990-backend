package com.rgapps.data.controller.resource;

import com.rgapps.data.model.RawData;

import java.util.HashMap;

public class DataView extends HashMap<String, Object> {
    public static DataView fromRawData(RawData data) {
        DataView dataView = new DataView();
        dataView.put("id", data.getId());
        dataView.putAll(data.getProps());
        return dataView;
    }
}
