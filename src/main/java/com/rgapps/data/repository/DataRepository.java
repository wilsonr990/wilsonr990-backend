package com.rgapps.data.repository;

import com.rgapps.data.model.RawData;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface DataRepository extends ReactiveMongoRepository<RawData, UUID> {
    Flux<RawData> findAllByResourceName(String resourceName);
    Mono<RawData> findByIdAndResourceName(UUID id, String resourceName);
}
