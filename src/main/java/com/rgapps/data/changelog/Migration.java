package com.rgapps.data.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.rgapps.common.config.SimpleMongoTemplateWrapper;
import com.rgapps.data.model.RawData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.Map;

@ChangeLog(order = "005")
@Slf4j
public class Migration {
    @ChangeSet(order = "001", id = "fixCreatedDateAndProductsTag", author = "Wilson")
    public void fixCreatedDateAndProductsTag(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("fix fix Created Date And Products Tag");
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        List<RawData> dataWithoutCreatedDate = mongoTemplate.find(
                new Query().addCriteria(new Criteria("createdDate").is(null)), RawData.class);
        dataWithoutCreatedDate.forEach(data ->
                mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("id").is(data.getId())),
                        new Update().set("createdDate", data.getUpdatedDate()), RawData.class));

        List<RawData> products = mongoTemplate.find(
                new Query().addCriteria(new Criteria("resourceName").is("products")), RawData.class);
        products.forEach(data -> {
            Map<String, Object> props = data.getProps();
            props.remove("tagText");
            props.put("tagId", "");
            props.put("currency", "AED");
            mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("id").is(data.getId())),
                    new Update().set("props", props), RawData.class);
        });
    }
}
