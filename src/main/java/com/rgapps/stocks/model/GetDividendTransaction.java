package com.rgapps.stocks.model;

import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class GetDividendTransaction extends StockTransaction {
    protected MoneyAmount dividendFee;
    protected MoneyAmount earningsPerShare;

    public GetDividendTransaction(UUID accountId,
                                  Stock stock,
                                  int numberOfShares,
                                  MoneyAmount earningsPerShare,
                                  LocalDate transactionDate,
                                  MoneyAmount dividendFee,
                                  Status status) {
        super(accountId, Type.DIVIDEND, stock, numberOfShares, transactionDate, status);
        this.dividendFee = dividendFee;
        this.earningsPerShare = earningsPerShare;
    }

    public MoneyAmount getTotalEarnings() {
        return earningsPerShare.multipliedBy(this.numberOfShares).plus(dividendFee);
    }

    @Override
    public MoneyAmount getAdjustedPrice() {
        return getTotalEarnings().dividedBy(numberOfShares);
    }
}
