package com.rgapps.stocks.model.values;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "stocks")
public class Stock {
    @Id
    private String code;

    public Stock() {
    }

    public Stock(String code) {
        this.code = code;
    }
}
