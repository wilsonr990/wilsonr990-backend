package com.rgapps.stocks.model.values;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Currency {
    public static final Currency ANY = new Currency("ANY");

    @Id
    private String code;

    public Currency() {
    }

    public Currency(String code) {
        this.code = code;
    }
}
