package com.rgapps.stocks.model;

import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Data
@Document(collection = "monthly-profit-summary")
public class MonthlyProfitSummary {
    @EqualsAndHashCode.Exclude
    private LocalDateTime updatedDate;

    @Id
    private String id;
    private LocalDate month;
    private Stock stock;
    private MoneyAmount averageBoughtPrice;
    private MoneyAmount averageSoldPrice;
    private int numberOfSharesSold;
    private UUID accountId;

    public MonthlyProfitSummary(LocalDate month,
                                Stock stock,
                                MoneyAmount averageBoughtPrice,
                                MoneyAmount averageSoldPrice,
                                int numberOfSharesSold,
                                UUID accountId) {
        assert averageBoughtPrice.getCurrency().equals(averageSoldPrice.getCurrency());

        this.month = month;
        this.stock = stock;
        this.averageBoughtPrice = averageBoughtPrice;
        this.averageSoldPrice = averageSoldPrice;
        this.numberOfSharesSold = numberOfSharesSold;
        this.accountId = accountId;

        updatedDate = LocalDateTime.now();
        id = calculateId(month, stock, accountId);
    }

    public static String calculateId(LocalDate month, Stock stock, UUID accountId) {
        return String.format("%s-%s-%s", month.format(DateTimeFormatter.ofPattern("MM-yyyy")), stock.getCode(), accountId);
    }

    public static String calculateId(StockTransaction transaction) {
        return calculateId(transaction.transactionDate, transaction.stock, transaction.getAccountId());
    }

    public static MonthlyProfitSummary empty(LocalDate date, Stock stock, UUID accountId) {
        return new MonthlyProfitSummary(date.withDayOfMonth(1),
                stock,
                MoneyAmount.zeroAmount,
                MoneyAmount.zeroAmount,
                0,
                accountId);
    }

    public static MonthlyProfitSummary empty(StockTransaction transaction) {
        return empty(transaction.transactionDate, transaction.stock, transaction.getAccountId());
    }

    public MonthlyProfitSummary updateSummaryWhenSell(StockJournal stockJournal, SellStockTransaction transaction) {
        assert stock.getCode().equals(transaction.getStock().getCode());
        assert transaction.getType().equals(StockTransaction.Type.SELL);
        assert transaction.getStatus().equals(StockTransaction.Status.SUCCESS);
        assert month.equals(transaction.transactionDate.withDayOfMonth(1));
        assert transaction.getAccountId().equals(accountId);

        int totalShares = numberOfSharesSold + transaction.numberOfShares;
        this.averageBoughtPrice = getTotalInvested()
                .plus(stockJournal.getAveragePricePerShare().multipliedBy(transaction.numberOfShares))
                .dividedBy(totalShares);
        this.averageSoldPrice = getTotalRevenue().plus(transaction.getTotalReceived()).dividedBy(totalShares);
        this.numberOfSharesSold = totalShares;

        updatedDate = LocalDateTime.now();
        return this;
    }

    public MonthlyProfitSummary updateSummaryWhenDividend(StockJournal stockJournal, GetDividendTransaction transaction) {
        assert stock.getCode().equals(transaction.getStock().getCode());
        assert transaction.getType().equals(StockTransaction.Type.DIVIDEND);
        assert transaction.getStatus().equals(StockTransaction.Status.SUCCESS);
        assert month.equals(transaction.transactionDate.withDayOfMonth(1));
        assert transaction.getAccountId().equals(accountId);

        int totalShares = numberOfSharesSold + transaction.numberOfShares;
        this.averageBoughtPrice = getTotalInvested()
                .plus(stockJournal.getAveragePricePerShare().multipliedBy(transaction.numberOfShares))
                .dividedBy(totalShares);
        this.averageSoldPrice = getTotalRevenue().plus(transaction.getTotalEarnings()).dividedBy(totalShares);
        this.numberOfSharesSold = totalShares;

        updatedDate = LocalDateTime.now();
        return this;
    }

    public MonthlyProfitSummary updateWhenDeleteSellTransaction(StockJournal stockJournal, SellStockTransaction transaction) {
        assert stock.getCode().equals(transaction.getStock().getCode());
        assert transaction.getType().equals(StockTransaction.Type.SELL);
        assert transaction.getStatus().equals(StockTransaction.Status.DELETED);
        assert month.equals(transaction.transactionDate.withDayOfMonth(1));
        assert transaction.getAccountId().equals(accountId);

        int totalShares = numberOfSharesSold - transaction.numberOfShares;
        this.averageBoughtPrice = getTotalInvested()
                .minus(stockJournal.getAveragePricePerShare().multipliedBy(transaction.numberOfShares))
                .dividedBy(totalShares);
        this.averageSoldPrice = getTotalRevenue().minus(transaction.getTotalReceived()).dividedBy(totalShares);
        this.numberOfSharesSold = totalShares;

        this.updatedDate = LocalDateTime.now();
        return this;
    }

    public MonthlyProfitSummary updateWhenDeleteDividendTransaction(StockJournal stockJournal, GetDividendTransaction transaction) {
        assert stock.getCode().equals(transaction.getStock().getCode());
        assert transaction.getType().equals(StockTransaction.Type.DIVIDEND);
        assert transaction.getStatus().equals(StockTransaction.Status.DELETED);
        assert month.equals(transaction.transactionDate.withDayOfMonth(1));
        assert transaction.getAccountId().equals(accountId);

        int totalShares = numberOfSharesSold - transaction.numberOfShares;
        this.averageBoughtPrice = getTotalInvested()
                .minus(stockJournal.getAveragePricePerShare().multipliedBy(transaction.numberOfShares))
                .dividedBy(totalShares);
        this.averageSoldPrice = getTotalRevenue().minus(transaction.getTotalEarnings()).dividedBy(totalShares);
        this.numberOfSharesSold = totalShares;

        this.updatedDate = LocalDateTime.now();
        return this;
    }

    public MoneyAmount getTotalProfit() {
        return averageSoldPrice.minus(averageBoughtPrice).multipliedBy(numberOfSharesSold);
    }

    public double getProfitPercentage() {
        return getTotalProfit().multipliedBy(100).dividedBy(getTotalInvested()).getValue();
    }

    public MoneyAmount getTotalInvested() {
        return averageBoughtPrice.multipliedBy(numberOfSharesSold);
    }

    public MoneyAmount getTotalRevenue() {
        return averageSoldPrice.multipliedBy(numberOfSharesSold);
    }

    public Currency getCurrency() {
        return averageSoldPrice.getCurrency();
    }
}
