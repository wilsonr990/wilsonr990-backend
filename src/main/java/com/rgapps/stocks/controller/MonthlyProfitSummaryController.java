package com.rgapps.stocks.controller;

import com.rgapps.common.validator.IsUUID;
import com.rgapps.stocks.controller.resource.SummaryForMonthView;
import com.rgapps.stocks.service.MonthlyProfitSummaryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/stocks/transactions/profitSummary")
@CrossOrigin
@Validated
@RequiredArgsConstructor
public class MonthlyProfitSummaryController {
    private final MonthlyProfitSummaryService monthlyProfitSummaryService;

    @GetMapping("/{year}/{month}")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Flux<SummaryForMonthView> getSummaryForMonth(@Min(0) @PathVariable int year,
                                                        @Min(0) @Max(12) @PathVariable int month,
                                                        @Valid @RequestParam @IsUUID @NotBlank String accountId) {
        return monthlyProfitSummaryService.getSummaryViewForMonth(LocalDate.of(year, month, 1), UUID.fromString(accountId));
    }
}
