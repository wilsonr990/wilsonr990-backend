package com.rgapps.stocks.controller;

import com.rgapps.common.validator.IsUUID;
import com.rgapps.stocks.controller.resource.BuyStockResource;
import com.rgapps.stocks.controller.resource.GetDividendResource;
import com.rgapps.stocks.controller.resource.SellStockResource;
import com.rgapps.stocks.controller.resource.StockTransactionView;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.service.StockTransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/stocks/transactions")
@CrossOrigin
@RequiredArgsConstructor
public class StockTransactionController {
    private final StockTransactionService stockTransactionService;

    @PostMapping("/buy")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<BuyStockTransaction> buyStock(
            @RequestParam @Valid @IsUUID @NotBlank String accountId,
            @Valid @RequestBody BuyStockResource buyResource) {
        return stockTransactionService.buyStock(UUID.fromString(accountId), buyResource);
    }

    @PostMapping("/sell")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<SellStockTransaction> sellStock(
            @RequestParam @Valid @IsUUID @NotBlank String accountId,
            @Valid @RequestBody SellStockResource sellResource) {
        return stockTransactionService.sellStock(UUID.fromString(accountId), sellResource);
    }

    @PostMapping("/getDividend")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<GetDividendTransaction> getDividend(
            @RequestParam @Valid @IsUUID @NotBlank String accountId,
            @Valid @RequestBody GetDividendResource getDividendResource) {
        return stockTransactionService.getDividend(getDividendResource, UUID.fromString(accountId));
    }

    @DeleteMapping("/last")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<StockTransaction> deleteLast(@RequestParam(required = false) String stockCode) {
        return stockCode == null ?
                stockTransactionService.deleteLast() :
                stockTransactionService.deleteLastForStock(stockCode);
    }

    @GetMapping("")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Flux<StockTransactionView> getStockTransactionsView() {
        return stockTransactionService.getStockTransactionsView();
    }
}
