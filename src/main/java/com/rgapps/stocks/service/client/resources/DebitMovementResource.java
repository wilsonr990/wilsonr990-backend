package com.rgapps.stocks.service.client.resources;

import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import lombok.Data;

import java.util.UUID;

import static com.rgapps.stocks.service.client.resources.DebitMovementResource.DebitType.STOCKS_DELETE_TRANSACTION;
import static com.rgapps.stocks.service.client.resources.DebitMovementResource.DebitType.STOCKS_INVESTMENT;

@Data
public class DebitMovementResource {
    public enum DebitType {
        STOCKS_INVESTMENT,
        STOCKS_DELETE_TRANSACTION
    }

    private final Double debitAmount;
    private final String currencyCode;
    private final String dateDebited;
    private final UUID externalId;
    private final DebitType debitType;

    public static DebitMovementResource fromBuyTransaction(BuyStockTransaction transaction) {
        return new DebitMovementResource(
                transaction.getTotalSpent().getValue(),
                transaction.getTotalSpent().getCurrency().getCode(),
                transaction.getTransactionDate().toString(),
                transaction.getId(),
                STOCKS_INVESTMENT
        );
    }

    public static DebitMovementResource fromSellDeletedTransaction(SellStockTransaction transaction) {
        return new DebitMovementResource(
                transaction.getTotalReceived().getValue(),
                transaction.getTotalReceived().getCurrency().getCode(),
                transaction.getTransactionDate().toString(),
                transaction.getId(),
                STOCKS_DELETE_TRANSACTION
        );
    }

    public static DebitMovementResource fromDividendDeletedTransaction(GetDividendTransaction transaction) {
        return new DebitMovementResource(
                transaction.getTotalEarnings().getValue(),
                transaction.getTotalEarnings().getCurrency().getCode(),
                transaction.getTransactionDate().toString(),
                transaction.getId(),
                STOCKS_DELETE_TRANSACTION
        );
    }
}
