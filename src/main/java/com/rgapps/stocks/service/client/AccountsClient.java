package com.rgapps.stocks.service.client;

import com.rgapps.common.exception.apiexceptions.ClientCallException;
import com.rgapps.common.security.AuthenticationUtils;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.service.client.resources.AccountSummaryView;
import com.rgapps.stocks.service.client.resources.CreditMovementResource;
import com.rgapps.stocks.service.client.resources.DebitMovementResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
public class AccountsClient {
    @Value("${client.hosts.accounts}")
    private String accountsServiceHost;

    private final WebClient webClient = WebClient.create();

    public Mono<AccountSummaryView> accountSummary(UUID accountId) {
        String uri = String.format("%s/api/accounts/%s/summary", accountsServiceHost, accountId);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token ->
                        webClient.get()
                                .uri(uri)
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                                .accept(MediaType.APPLICATION_JSON)
                                .retrieve()
                                .bodyToMono(AccountSummaryView.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, uri));
    }

    public Mono<Object> debit(BuyStockTransaction transaction) {
        String uri = String.format("%s/api/accounts/%s/movements/debit", accountsServiceHost, transaction.getAccountId());
        DebitMovementResource body = DebitMovementResource.fromBuyTransaction(transaction);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token -> webClient.post()
                        .uri(uri)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(body)
                        .retrieve()
                        .bodyToMono(Object.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, uri, body.toString()));
    }

    public Mono<Object> debitOnDeleteSellTransaction(SellStockTransaction transaction) {
        String uri = String.format("%s/api/accounts/%s/movements/debit", accountsServiceHost, transaction.getAccountId());
        DebitMovementResource body = DebitMovementResource.fromSellDeletedTransaction(transaction);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token -> webClient.post()
                        .uri(uri)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(body)
                        .retrieve()
                        .bodyToMono(Object.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, uri, body.toString()));
    }

    public Mono<Object> debitOnDeleteDividendTransaction(GetDividendTransaction transaction) {
        String uri = String.format("%s/api/accounts/%s/movements/debit", accountsServiceHost, transaction.getAccountId());
        DebitMovementResource body = DebitMovementResource.fromDividendDeletedTransaction(transaction);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token -> webClient.post()
                        .uri(uri)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(body)
                        .retrieve()
                        .bodyToMono(Object.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, uri, body.toString()));
    }

    public Mono<Object> creditOnSell(SellStockTransaction transaction) {
        String uri = String.format("%s/api/accounts/%s/movements/credit", accountsServiceHost, transaction.getAccountId());
        CreditMovementResource body = CreditMovementResource.fromSellTransaction(transaction);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token -> webClient.post()
                        .uri(uri)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(body)
                        .retrieve()
                        .bodyToMono(Object.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, uri, body.toString()));
    }

    public Mono<Object> creditOnDelete(BuyStockTransaction transaction) {
        String uri = String.format("%s/api/accounts/%s/movements/credit", accountsServiceHost, transaction.getAccountId());
        CreditMovementResource body = CreditMovementResource.fromBuyDeletedTransaction(transaction);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token -> webClient.post()
                        .uri(uri)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(body)
                        .retrieve()
                        .bodyToMono(Object.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, uri, body.toString()));
    }

    public Mono<Object> creditOnDividend(GetDividendTransaction transaction) {
        String uri = String.format("%s/api/accounts/%s/movements/credit", accountsServiceHost, transaction.getAccountId());
        CreditMovementResource body = CreditMovementResource.fromGetDividendTransaction(transaction);
        return AuthenticationUtils.getJwtToken()
                .flatMap(token -> webClient.post()
                        .uri(uri)
                        .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                        .accept(MediaType.APPLICATION_JSON)
                        .bodyValue(body)
                        .retrieve()
                        .bodyToMono(Object.class))
                .onErrorMap(WebClientException.class, e -> new ClientCallException(e, body.toString()));
    }
}
