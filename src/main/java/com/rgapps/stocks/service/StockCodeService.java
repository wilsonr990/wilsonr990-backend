package com.rgapps.stocks.service;

import com.rgapps.stocks.model.values.Stock;
import com.rgapps.stocks.repository.StockValueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StockCodeService {
    private final StockValueRepository stockValueRepository;

    public Mono<Stock> saveStock(Stock stock) {
        return stockValueRepository.save(stock)
                .flatMap(this::onStockSaved);
    }

    private Mono<Stock> onStockSaved(Stock code) {
        return Mono.just(code);
    }

    public Mono<List<String>> findCodes() {
        return stockValueRepository.findAll().map(Stock::getCode).collectList();
    }
}
