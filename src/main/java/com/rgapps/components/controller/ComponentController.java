package com.rgapps.components.controller;

import com.rgapps.components.controller.resource.ComponentResource;
import com.rgapps.components.controller.resource.ComponentView;
import com.rgapps.components.model.Component;
import com.rgapps.components.service.ComponentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;
import javax.validation.Valid;

@RestController
@RequestMapping("api/components")
@CrossOrigin
@RequiredArgsConstructor
public class ComponentController {
    private final ComponentService componentService;

    @PostMapping("")
    public Mono<Component> createComponent(@Valid @RequestBody ComponentResource resource) {
        return componentService.create(resource);
    }

    @DeleteMapping("")
    public Mono<Component> deleteComponent(@RequestParam String id) {
        return componentService.delete(UUID.fromString(id));
    }

    @GetMapping("")
    public Flux<ComponentView> getComponentsView() {
        return componentService.getComponents()
                .map(ComponentView::fromComponent);
    }
}
