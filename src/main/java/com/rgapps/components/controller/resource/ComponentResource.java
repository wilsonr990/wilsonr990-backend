package com.rgapps.components.controller.resource;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.rgapps.components.model.Component;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = BusinessCardResource.class, name = "BUSINESS_CARD")})
public abstract class ComponentResource {
    private String type;
    @NotEmpty
    protected String componentName;

    public abstract Component toComponent();
}
