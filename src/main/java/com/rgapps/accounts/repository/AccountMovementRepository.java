package com.rgapps.accounts.repository;

import com.rgapps.accounts.model.AccountMovement;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.UUID;

@Repository
public interface AccountMovementRepository extends ReactiveMongoRepository<AccountMovement, UUID> {
    Flux<AccountMovement> findByStatusAndTypeInAndAccountInOrderByMovementDateDesc(
            AccountMovement.Status status, List<String> types, List<UUID> accounts);

    Flux<AccountMovement> findByStatusAndAccountInOrderByMovementDateDesc(
            AccountMovement.Status status, List<UUID> accounts);
}
