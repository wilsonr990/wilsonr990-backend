package com.rgapps.accounts.repository;

import com.rgapps.accounts.model.AccountSummary;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AccountSummaryRepository extends ReactiveMongoRepository<AccountSummary, UUID> {
}
