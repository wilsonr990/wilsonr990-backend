package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.common.validator.InThePastOrPresent;
import com.rgapps.common.validator.IsEnum;
import com.rgapps.common.validator.IsLocalDate;
import com.rgapps.common.validator.IsUUID;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.rgapps.accounts.model.values.Currency.ANY;

@Data
public class CreditMovementResource {
    @NotNull
    @Min(0)
    private Double creditAmount;
    @NotEmpty
    private String currencyCode;
    @NotEmpty
    @IsLocalDate
    @InThePastOrPresent
    private String dateCredited;
    @NotEmpty
    @IsEnum(CreditAccountMovement.CreditType.class)
    private String creditType;
    @NotEmpty
    @IsUUID
    protected String externalId;

    public CreditAccountMovement toAccountMovement(AccountMovement.Status status, UUID accountId) {
        return new CreditAccountMovement(
                accountId,
                new MoneyAmount(creditAmount, new Currency(currencyCode)),
                LocalDate.parse(dateCredited),
                status,
                CreditAccountMovement.CreditType.valueOf(creditType),
                UUID.fromString(externalId));
    }

    public boolean validateIsCorrectCurrency(Currency currency) {
        if (!currency.equals(ANY) && !this.currencyCode.equals(currency.getCode())) {
            throw new UnexpectedValueException("Currency", currency.getCode(), this.currencyCode);
        }
        return true;
    }
}
