package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.common.validator.InThePastOrPresent;
import com.rgapps.common.validator.IsEnum;
import com.rgapps.common.validator.IsLocalDate;
import com.rgapps.common.validator.IsUUID;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.rgapps.accounts.model.values.Currency.ANY;

@Data
public class DebitMovementResource {
    @NotNull
    @Min(0)
    private Double debitAmount;
    @NotEmpty
    private String currencyCode;
    @NotEmpty
    @IsLocalDate
    @InThePastOrPresent
    private String dateDebited;
    @NotEmpty
    @IsEnum(DebitAccountMovement.DebitType.class)
    private String debitType;
    @NotEmpty
    @IsUUID
    protected String externalId;

    public DebitAccountMovement toAccountMovement(AccountMovement.Status status, UUID account) {
        return new DebitAccountMovement(
                account,
                new MoneyAmount(debitAmount, new Currency(currencyCode)),
                LocalDate.parse(dateDebited),
                status,
                DebitAccountMovement.DebitType.valueOf(debitType),
                UUID.fromString(externalId));
    }

    public boolean validateIsCorrectCurrency(Currency currency) {
        if (!currency.equals(ANY) && !this.currencyCode.equals(currency.getCode())) {
            throw new UnexpectedValueException("Currency", currency.getCode(), this.currencyCode);
        }
        return true;
    }

    public boolean validateHasEnoughMoneyToDebit(double currentAmount) {
        if (debitAmount > currentAmount) {
            throw new UnexpectedValueException("Money to Debit", String.format("<= %f", currentAmount), debitAmount);
        }
        return true;
    }
}
