package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.values.Currency;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;
import javax.validation.constraints.NotEmpty;

@Data
@Document(collection = "account")
public class AccountResource {
    @NotEmpty
    private String accountName;
    @NotEmpty
    private String currency;

    public Account toAccount(UUID userId) {
        return new Account(accountName, new Currency(currency), userId);
    }
}
