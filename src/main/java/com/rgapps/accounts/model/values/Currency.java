package com.rgapps.accounts.model.values;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Document(collection = "currencies")
public class Currency {
    public static final Currency ANY = new Currency("ANY");

    @Id
    @NonNull
    private String code;
}
