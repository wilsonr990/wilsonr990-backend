package com.rgapps.accounts.model;

import com.rgapps.accounts.model.values.MoneyAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class CreditAccountMovement extends AccountMovement {
    public enum CreditType {
        STOCKS_REVENUE,
        STOCKS_DIVIDEND,
        STOCKS_DELETE_TRANSACTION
    }

    protected CreditType creditType;
    protected UUID externalId;

    public CreditAccountMovement(UUID account,
                                 MoneyAmount amount,
                                 LocalDate movementDate,
                                 Status status,
                                 CreditType creditType,
                                 UUID externalId) {
        super(account, Type.CREDIT, amount, movementDate, status);
        this.creditType = creditType;
        this.externalId = externalId;
    }
}
