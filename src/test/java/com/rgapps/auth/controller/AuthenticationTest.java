package com.rgapps.auth.controller;

import com.rgapps.auth.IntegrationTest;
import com.rgapps.auth.controller.resource.AuthResponse;
import com.rgapps.auth.controller.resource.SignupResource;
import com.rgapps.auth.controller.resource.UserView;
import com.rgapps.auth.datahelper.DataHelper;
import com.rgapps.auth.datahelper.ObjectValidations;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Disabled("Embedded mongo not working inside docker")
class AuthenticationTest extends IntegrationTest {
    @ParameterizedTest
    @MethodSource("com.rgapps.auth.datahelper.DataHelper#registerUserFailBodyCases")
    public void registerUserFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = webTestClient.post()
                .uri("/api/auth/signup")
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(User.class).collectList().block()).hasSize(0);
    }

    @Test
    public void registerUserCorrectly() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();

        LocalDateTime start = LocalDateTime.now();
        UserView user = signup(DataHelper.DEFAULT_SIGNUP)
                .expectBody(UserView.class)
                .returnResult()
                .getResponseBody();
        LocalDateTime end = LocalDateTime.now();

        ObjectValidations.validateUserView(user, "user", List.of(Role.RoleType.ROLE_USER));

        List<User> users = mongoTemplate.findAll(User.class).collectList().block();
        assertThat(users).isNotNull().hasSize(1);
        assertThat(users.get(0)).isNotNull();
        ObjectValidations.validateUser(users.get(0), "user", List.of(Role.RoleType.ROLE_USER), start, end);
    }

    @Test
    public void registerUserFailsIfRoleIsNotFound() {
        ModelErrorResponse<SignupResource> response = webTestClient.post()
                .uri("/api/auth/signup")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SIGNUP)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSignupResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, NOT_FOUND,
                "NotFoundException",
                "Not found 'role' ({type=ROLE_USER})");
        assertThat(response).isNotNull();
        SignupResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSignUpResource(erroredObject, "user", "password", List.of("ROLE_USER"));
        assertThat(mongoTemplate.findAll(User.class).collectList().block()).hasSize(0);
    }

    @Test
    public void registerUserFailsIfUserAlreadyExists() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();
        signup(DataHelper.DEFAULT_SIGNUP);

        ModelErrorResponse<SignupResource> response = webTestClient.post()
                .uri("/api/auth/signup")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SIGNUP)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSignupResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "DuplicatedValueException",
                "User 'user' already exists");

        assertThat(response).isNotNull();
        SignupResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSignUpResource(erroredObject, "user", "password", List.of("ROLE_USER"));
        assertThat(mongoTemplate.findAll(User.class).collectList().block()).hasSize(1);
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.auth.datahelper.DataHelper#authenticateUserFailBodyCases")
    public void authenticateUserFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = webTestClient.post()
                .uri("/api/auth/signin")
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);
    }

    @Test
    public void authenticateUserCorrectly() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();
        signup(DataHelper.DEFAULT_SIGNUP);

        AuthResponse response = signin(DataHelper.DEFAULT_SIGNIN)
                .expectBody(AuthResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateAuthResponse(response);
    }

    @Test
    public void authenticateUserFailsIfUserIsNotFound() {
        ApiErrorResponse response = webTestClient.post()
                .uri("/api/auth/signin")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SIGNIN)
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, UNAUTHORIZED,
                "AuthenticationException",
                "User Not found");
    }

    @Test
    public void authenticateUserFailsIfPasswordIsIncorrect() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();
        signup(DataHelper.DEFAULT_SIGNUP);

        ApiErrorResponse response = webTestClient.post()
                .uri("/api/auth/signin")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.SIGNIN_INCORRECT_PASSWORD)
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, UNAUTHORIZED,
                "AuthenticationException",
                "Invalid Password");
    }

    @Test
    public void listUsersCorrectly() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();
        mongoTemplate.save(new Role(Role.RoleType.ROLE_ADMIN)).block();
        signup(DataHelper.SIGNUP_ADMIN);
        signup(DataHelper.DEFAULT_SIGNUP);

        AuthResponse auth = signin(DataHelper.SIGNIN_ADMIN)
                .expectBody(AuthResponse.class)
                .returnResult()
                .getResponseBody();

        assertThat(auth).isNotNull();
        List<UserView> users = webTestClient.get()
                .uri("/api/auth/users")
                .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", auth.getToken()))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(UserView.class)
                .returnResult()
                .getResponseBody();

        assertThat(users).isNotNull();
        assertThat(users).hasSize(2);
        ObjectValidations.validateUserView(users.get(0), "admin", List.of(Role.RoleType.ROLE_ADMIN));
        ObjectValidations.validateUserView(users.get(1), "user", List.of(Role.RoleType.ROLE_USER));
    }

    @Test
    public void listUsersFailsForAnUserWithoutAdminRole() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();
        mongoTemplate.save(new Role(Role.RoleType.ROLE_ADMIN)).block();
        signup(DataHelper.SIGNUP_ADMIN);
        signup(DataHelper.DEFAULT_SIGNUP);

        AuthResponse auth = signin(DataHelper.DEFAULT_SIGNIN)
                .expectBody(AuthResponse.class)
                .returnResult()
                .getResponseBody();

        assertThat(auth).isNotNull();
        ApiErrorResponse response = webTestClient.get()
                .uri("/api/auth/users")
                .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", auth.getToken()))
                .exchange()
                .expectStatus().isEqualTo(FORBIDDEN)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, FORBIDDEN,
                "AccessDeniedException",
                "Denied");
    }

    @Test
    public void listUsersFailsIfNotAuthenticated() {
        webTestClient.get()
                .uri("/api/auth/users")
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody()
                .isEmpty();
    }

    @Test
    public void getCurrentUserCorrectly() {
        mongoTemplate.save(new Role(Role.RoleType.ROLE_USER)).block();
        signup(DataHelper.DEFAULT_SIGNUP);

        AuthResponse auth = signin(DataHelper.DEFAULT_SIGNIN)
                .expectBody(AuthResponse.class)
                .returnResult()
                .getResponseBody();

        assertThat(auth).isNotNull();
        UserView users = webTestClient.get()
                .uri("/api/auth/me")
                .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", auth.getToken()))
                .exchange()
                .expectStatus().isOk()
                .expectBody(UserView.class)
                .returnResult()
                .getResponseBody();

        assertThat(users).isNotNull();
        ObjectValidations.validateUserView(users, "user", List.of(Role.RoleType.ROLE_USER));
    }

    @Test
    public void getCurrentUserFailsIfNotAuthenticated() {
        webTestClient.get()
                .uri("/api/auth/me")
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody()
                .isEmpty();
    }
}
