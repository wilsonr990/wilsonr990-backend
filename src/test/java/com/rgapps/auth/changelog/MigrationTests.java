package com.rgapps.auth.changelog;

import com.rgapps.auth.IntegrationTest;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class MigrationTests extends IntegrationTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    Migration migration = new Migration();

    @Test
    public void createDefaultAccount() {
        migration.insertAdminUserAndRoles(() -> mongoTemplate);

        List<Role> roles = mongoTemplate.find(new Query(), Role.class);
        assertThat(roles).hasSize(3);
        assertThat(roles.stream().map(Role::getType))
                .containsAll(List.of(Role.RoleType.ROLE_ADMIN, Role.RoleType.ROLE_MODERATOR, Role.RoleType.ROLE_USER));
        List<User> users = mongoTemplate.find(new Query(), User.class);
        assertThat(users).hasSize(1);
        assertThat(users.get(0).getUsername()).isEqualTo("admin");
        assertThat(users.get(0).getRoles()).hasSize(1);
        assertThat(users.get(0).getRoles().stream().map(Role::getType)).contains(Role.RoleType.ROLE_ADMIN);
    }

    @Test
    public void changeModuleNames() {
        User user = mongoTemplate.save(new User("DEFAULT", "pass", Collections.emptySet()));
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(user.getId())),
                new Update().set("_class", "com.wilsonr990.page.auth.model.User"), "user");
        Role role = mongoTemplate.save(new Role(Role.RoleType.ROLE_USER));
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(role.getId())),
                new Update().set("_class", "com.wilsonr990.page.auth.model.Role"), "role");

        migration.changeModuleNamesAuth(() -> mongoTemplate);

        User updatedUser =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(user.getId())), User.class);
        Role updatedRole =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(role.getId())), Role.class);
        assertThat(updatedUser).isEqualTo(user);
        assertThat(updatedRole).isEqualTo(role);
    }
}
