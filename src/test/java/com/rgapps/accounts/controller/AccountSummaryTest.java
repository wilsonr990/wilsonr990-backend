package com.rgapps.accounts.controller;

import com.rgapps.accounts.IntegrationTest;
import com.rgapps.accounts.controller.resource.AccountSummaryView;
import com.rgapps.accounts.datahelper.DataHelper;
import com.rgapps.accounts.datahelper.ModelData;
import com.rgapps.accounts.datahelper.ObjectValidations;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.commons.AuthenticatedUsers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class AccountSummaryTest extends IntegrationTest {

    @Test
    public void accountSummaryIsUpdatedAfterDeposit() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();
        DepositAccountMovement response = deposit(DataHelper.DEFAULT_DEPOSIT)
                .expectBody(DepositAccountMovement.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        assertThat(response).isNotNull();
        AccountSummary accountSummary =
                mongoTemplate.findById(account.getId(), AccountSummary.class).block();
        ObjectValidations.validateAccountSummary(accountSummary,
                account.getId(),
                "AED",
                "DEFAULT",
                10.0,
                "2020-12-07",
                "1970-01-01");
    }

    @Test
    public void accountSummaryIsUpdatedAfterWithDraw() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();
        deposit(DataHelper.DEFAULT_DEPOSIT);
        WithdrawAccountMovement response = withdraw(DataHelper.DEFAULT_WITHDRAW)
                .expectBody(WithdrawAccountMovement.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        assertThat(response).isNotNull();
        AccountSummary accountSummary =
                mongoTemplate.findById(account.getId(), AccountSummary.class).block();
        ObjectValidations.validateAccountSummary(accountSummary,
                account.getId(),
                "AED",
                "DEFAULT",
                0.0,
                "2020-12-07",
                "2020-12-07");
    }

    @Test
    public void findAccountSummaryReturnsNotFoundOnAccountNotExistent() {
        ApiErrorResponse response = authenticatedWebTestClient.get()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/summary")
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        com.rgapps.auth.datahelper.ObjectValidations.validateError(response,
                HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", AuthenticatedUsers.normalUserId));
    }

    @Test
    public void findAccountSummaryReturnsEmptyIfSummaryNotFoundForAccount() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();
        AccountSummaryView response = authenticatedWebTestClient.get()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/summary")
                .exchange()
                .expectStatus().isOk()
                .expectBody(AccountSummaryView.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateAccountSummaryView(response,
                account.getId(),
                "AED",
                "DEFAULT",
                0.0,
                "");
    }

    @Test
    public void findAccountSummaryCorrectly() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();
        deposit(DataHelper.DEFAULT_DEPOSIT);
        AccountSummaryView response = authenticatedWebTestClient.get()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/summary")
                .exchange()
                .expectStatus().isOk()
                .expectBody(AccountSummaryView.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateAccountSummaryView(response,
                account.getId(),
                "AED",
                "DEFAULT",
                10.0,
                "2020-12-07");
    }

    @Test
    public void findAccountSummaryFailsIfNotAuthenticated() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();
        webTestClient.get()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/summary")
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void findAccountSummaryFailsIfAccountIsNotOwnedByCurrentUser() {
        mongoTemplate.save(ModelData.accountForOtherUser).block();
        ApiErrorResponse response = authenticatedWebTestClient.get()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/summary")
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        com.rgapps.auth.datahelper.ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", AuthenticatedUsers.normalUserId));
    }
}
