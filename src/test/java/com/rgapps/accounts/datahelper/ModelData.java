package com.rgapps.accounts.datahelper;

import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import com.rgapps.commons.AuthenticatedUsers;

import java.time.LocalDate;
import java.util.UUID;

public class ModelData {
    public static Account defaultAccount = new Account("DEFAULT", new Currency("AED"), AuthenticatedUsers.normalUserId);
    public static Account accountForOtherUser = new Account("DEFAULT", new Currency("AED"), AuthenticatedUsers.normalUserId2);
    public static Currency defaultCurrency = new Currency("AED");
    public static MoneyAmount defaultAmount = new MoneyAmount(10, defaultCurrency);
    public static LocalDate now = LocalDate.now();

    public static DepositAccountMovement defaultDepositAccountMovement() {
        return new DepositAccountMovement(
                UUID.randomUUID(),
                defaultAmount,
                now,
                AccountMovement.Status.SUCCESS);
    }

    public static WithdrawAccountMovement defaultWithdrawAccountMovement() {
        return new WithdrawAccountMovement(
                UUID.randomUUID(),
                defaultAmount,
                now,
                AccountMovement.Status.SUCCESS);
    }

    public static CreditAccountMovement defaultCreditAccountMovement() {
        return new CreditAccountMovement(
                UUID.randomUUID(),
                defaultAmount,
                now,
                AccountMovement.Status.SUCCESS,
                CreditAccountMovement.CreditType.STOCKS_REVENUE,
                UUID.randomUUID());
    }

    public static DebitAccountMovement defaultDebitAccountMovement() {
        return new DebitAccountMovement(
                UUID.randomUUID(),
                defaultAmount,
                now,
                AccountMovement.Status.SUCCESS,
                DebitAccountMovement.DebitType.STOCKS_INVESTMENT,
                UUID.randomUUID());
    }
}
