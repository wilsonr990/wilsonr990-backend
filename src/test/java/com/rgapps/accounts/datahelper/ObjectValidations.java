package com.rgapps.accounts.datahelper;

import com.rgapps.accounts.controller.resource.AccountMovementView;
import com.rgapps.accounts.controller.resource.AccountSummaryView;
import com.rgapps.accounts.controller.resource.AccountView;
import com.rgapps.accounts.controller.resource.CreditMovementResource;
import com.rgapps.accounts.controller.resource.DebitMovementResource;
import com.rgapps.accounts.controller.resource.DepositMovementResource;
import com.rgapps.accounts.controller.resource.WithdrawMovementResource;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import org.assertj.core.api.Assertions;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjectValidations {
    public static void validateError(ValidationErrorResponse response,
                                     HttpStatus status,
                                     String error,
                                     String message,
                                     Map<String, List<String>> expectedErrors) {
        assertThat(response).isNotNull();
        Assertions.assertThat(response.getStatus()).isEqualTo(status);
        Assertions.assertThat(response.getError()).isEqualTo(error);
        Assertions.assertThat(response.getMessage()).isEqualTo(message);
        Map<String, List<String>> detail = response.getErrors();
        assertThat(detail).hasSameSizeAs(expectedErrors);
        assertThat(detail.keySet()).hasSameElementsAs(expectedErrors.keySet());
        detail.forEach((field, errors) -> assertThat(errors).hasSameElementsAs(expectedErrors.get(field)));
    }

    public static void validateError(ModelErrorResponse<?> response,
                                     HttpStatus status,
                                     String error,
                                     String message) {
        assertThat(response).isNotNull();
        Assertions.assertThat(response.getStatus()).isEqualTo(status);
        Assertions.assertThat(response.getError()).isEqualTo(error);
        Assertions.assertThat(response.getMessage()).isEqualTo(message);
    }

    public static void validateError(ApiErrorResponse response,
                                     HttpStatus status,
                                     String error,
                                     String message) {
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(status);
        assertThat(response.getError()).isEqualTo(error);
        assertThat(response.getMessage()).isEqualTo(message);
    }

    public static void validateAccountView(AccountView movement,
                                           UUID id,
                                           String accountName,
                                           String currency) {
        assertThat(movement).isNotNull();
        assertThat(movement.getId()).isEqualTo(id);
        assertThat(movement.getAccountName()).isEqualTo(accountName);
        assertThat(movement.getCurrency()).isEqualTo(currency);
    }

    public static void validateAccountMovement(DepositAccountMovement movement,
                                               UUID accountId,
                                               AccountMovement.Status status,
                                               String currency,
                                               double pricePerShare,
                                               String movementDate,
                                               LocalDateTime createdIntervalStart,
                                               LocalDateTime createdIntervalEnd) {
        assertThat(movement).isNotNull();
        assertThat(movement.getId()).isNotNull();
        assertThat(movement.getType()).isEqualTo(AccountMovement.Type.DEPOSIT);
        assertThat(movement.getAccount()).isEqualTo(accountId);
        assertThat(movement.getStatus()).isEqualTo(status);
        Assertions.assertThat(movement.getAmount()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        assertThat(movement.getMovementDate()).isEqualTo(movementDate);
        assertThat(movement.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(movement.getUpdatedDate()).isEqualTo(movement.getCreatedDate());
    }

    public static void validateAccountMovement(CreditAccountMovement movement,
                                               UUID accountId,
                                               CreditAccountMovement.CreditType creditType,
                                               String externalId,
                                               AccountMovement.Status status,
                                               String currency,
                                               double pricePerShare,
                                               String movementDate,
                                               LocalDateTime createdIntervalStart,
                                               LocalDateTime createdIntervalEnd) {
        assertThat(movement).isNotNull();
        assertThat(movement.getId()).isNotNull();
        assertThat(movement.getType()).isEqualTo(AccountMovement.Type.CREDIT);
        assertThat(movement.getCreditType()).isEqualTo(creditType);
        assertThat(movement.getExternalId().toString()).isEqualTo(externalId);
        assertThat(movement.getAccount()).isEqualTo(accountId);
        assertThat(movement.getStatus()).isEqualTo(status);
        Assertions.assertThat(movement.getAmount()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        assertThat(movement.getMovementDate()).isEqualTo(movementDate);
        assertThat(movement.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(movement.getUpdatedDate()).isEqualTo(movement.getCreatedDate());
    }

    public static void validateAccountMovement(WithdrawAccountMovement movement,
                                               UUID accountId,
                                               AccountMovement.Status status,
                                               String currency,
                                               double pricePerShare,
                                               String movementDate,
                                               LocalDateTime createdIntervalStart,
                                               LocalDateTime createdIntervalEnd) {
        assertThat(movement).isNotNull();
        assertThat(movement.getId()).isNotNull();
        assertThat(movement.getType()).isEqualTo(AccountMovement.Type.WITHDRAW);
        assertThat(movement.getAccount()).isEqualTo(accountId);
        assertThat(movement.getStatus()).isEqualTo(status);
        Assertions.assertThat(movement.getAmount()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        assertThat(movement.getMovementDate()).isEqualTo(movementDate);
        assertThat(movement.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(movement.getUpdatedDate()).isEqualTo(movement.getCreatedDate());
    }

    public static void validateAccountMovement(DebitAccountMovement movement,
                                               UUID accountId,
                                               DebitAccountMovement.DebitType debitType,
                                               String externalId,
                                               AccountMovement.Status status,
                                               String currency,
                                               double pricePerShare,
                                               String movementDate,
                                               LocalDateTime createdIntervalStart,
                                               LocalDateTime createdIntervalEnd) {
        assertThat(movement).isNotNull();
        assertThat(movement.getId()).isNotNull();
        assertThat(movement.getType()).isEqualTo(AccountMovement.Type.DEBIT);
        assertThat(movement.getDebitType()).isEqualTo(debitType);
        assertThat(movement.getExternalId().toString()).isEqualTo(externalId);
        assertThat(movement.getAccount()).isEqualTo(accountId);
        assertThat(movement.getStatus()).isEqualTo(status);
        Assertions.assertThat(movement.getAmount()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        assertThat(movement.getMovementDate()).isEqualTo(movementDate);
        assertThat(movement.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(movement.getUpdatedDate()).isEqualTo(movement.getCreatedDate());
    }

    public static void validateDepositMovementResource(DepositMovementResource movement,
                                                       String currencyCode,
                                                       double depositAmount,
                                                       String dateDeposited) {
        assertThat(movement).isNotNull();
        assertThat(movement.getCurrencyCode()).isEqualTo(currencyCode);
        assertThat(movement.getDepositAmount()).isEqualTo(depositAmount);
        assertThat(movement.getDateDeposited()).isEqualTo(dateDeposited);
    }

    public static void validateWithdrawMovementResource(WithdrawMovementResource movement,
                                                        String currencyCode,
                                                        double withdrawAmount,
                                                        String dateWithdrawn) {
        assertThat(movement).isNotNull();
        assertThat(movement.getCurrencyCode()).isEqualTo(currencyCode);
        assertThat(movement.getWithdrawAmount()).isEqualTo(withdrawAmount);
        assertThat(movement.getDateWithdrawn()).isEqualTo(dateWithdrawn);
    }

    public static void validateCreditMovementResource(CreditMovementResource movement,
                                                      String creditType,
                                                      String externalId,
                                                      String currencyCode,
                                                      double creditAmount,
                                                      String dateDeposited) {
        assertThat(movement).isNotNull();
        assertThat(movement.getCreditType()).isEqualTo(creditType);
        assertThat(movement.getExternalId()).isEqualTo(externalId);
        assertThat(movement.getCurrencyCode()).isEqualTo(currencyCode);
        assertThat(movement.getCreditAmount()).isEqualTo(creditAmount);
        assertThat(movement.getDateCredited()).isEqualTo(dateDeposited);
    }

    public static void validateDebitMovementResource(DebitMovementResource movement,
                                                     String debitType,
                                                     String externalId,
                                                     String currencyCode,
                                                     double debitAmount,
                                                     String dateWithdrawn) {
        assertThat(movement).isNotNull();
        assertThat(movement.getDebitType()).isEqualTo(debitType);
        assertThat(movement.getExternalId()).isEqualTo(externalId);
        assertThat(movement.getCurrencyCode()).isEqualTo(currencyCode);
        assertThat(movement.getDebitAmount()).isEqualTo(debitAmount);
        assertThat(movement.getDateDebited()).isEqualTo(dateWithdrawn);
    }

    public static void validateAccountSummary(AccountSummary summary,
                                              UUID accountId,
                                              String currency,
                                              String accountName,
                                              double currentAmount,
                                              String lastDepositDate,
                                              String lastWithDrawDate) {
        assertThat(summary).isNotNull();
        assertThat(summary.getAccountId()).isEqualTo(accountId);
        assertThat(summary.getCurrency()).isEqualTo(new Currency(currency));
        assertThat(summary.getAccountName()).isEqualTo(accountName);
        assertThat(summary.getCurrentAmount()).isEqualTo(new MoneyAmount(currentAmount, new Currency(currency)));
        assertThat(summary.getLastDepositDate()).isEqualTo(lastDepositDate);
        assertThat(summary.getLastWithDrawDate()).isEqualTo(lastWithDrawDate);
    }

    public static void validateAccountSummaryView(AccountSummaryView summary,
                                                  UUID accountId,
                                                  String currency,
                                                  String accountName,
                                                  double currentAmount,
                                                  String lastDepositOrWithdrawalDate) {
        assertThat(summary).isNotNull();
        assertThat(summary.getAccountId()).isEqualTo(accountId);
        assertThat(summary.getCurrency()).isEqualTo(currency);
        assertThat(summary.getAccountName()).isEqualTo(accountName);
        assertThat(summary.getAmount()).isEqualTo(currentAmount);
        assertThat(summary.getLastDepositOrWithdrawalDate()).isEqualTo(lastDepositOrWithdrawalDate);
    }

    public static void validateAccountMovementView(AccountMovementView movement,
                                                   AccountMovement.Type movementType,
                                                   String currency,
                                                   double amount,
                                                   String lastDepositOrWithdrawalDate) {
        assertThat(movement).isNotNull();
        assertThat(movement.getCurrency()).isEqualTo(currency);
        assertThat(movement.getMovementType()).isEqualTo(movementType);
        assertThat(movement.getAmount()).isEqualTo(amount);
        assertThat(movement.getDate()).isEqualTo(lastDepositOrWithdrawalDate);
    }
}
