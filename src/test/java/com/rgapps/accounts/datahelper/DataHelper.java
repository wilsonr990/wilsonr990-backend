package com.rgapps.accounts.datahelper;

import com.rgapps.accounts.controller.resource.CreditMovementResource;
import com.rgapps.accounts.controller.resource.DebitMovementResource;
import com.rgapps.accounts.controller.resource.DepositMovementResource;
import com.rgapps.accounts.controller.resource.WithdrawMovementResource;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class DataHelper {
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseDepositMovementResource extends ModelErrorResponse<DepositMovementResource> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseWithdrawMovementResource extends ModelErrorResponse<WithdrawMovementResource> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseCreditMovementResource extends ModelErrorResponse<CreditMovementResource> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseDebitMovementResource extends ModelErrorResponse<DebitMovementResource> {
    }

    public static Stream<Arguments> createAccountFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "accountName", List.of("must not be empty"),
                                "currency", List.of("must not be empty")
                        )),
                Arguments.of("{\"accountName\": \"\", \"currency\": \"\"}",
                        Map.of(
                                "accountName", List.of("must not be empty"),
                                "currency", List.of("must not be empty")
                        )));
    }

    public static Stream<Arguments> depositFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "dateDeposited", List.of("must not be empty"),
                                "depositAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"currencyCode\": \"\", \"dateDeposited\": \"\", \"depositAmount\": \"\"}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "dateDeposited", List.of("must not be empty"),
                                "depositAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"currencyCode\": \"AED\", \"dateDeposited\": \"invalid\", \"depositAmount\": -1}",
                        Map.of(
                                "dateDeposited", List.of("must be a date in the past or in the present",
                                        "must be a valid YYYY-MM-DD date"),
                                "depositAmount", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"currencyCode\": \"AED\", \"dateDeposited\": \"3020-12-07\", \"depositAmount\": 1}",
                        Map.of(
                                "dateDeposited", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> creditFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "creditType", List.of("must not be empty"),
                                "externalId", List.of("must not be empty"),
                                "dateCredited", List.of("must not be empty"),
                                "creditAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"creditType\":\"\", \"externalId\":\"\", \"currencyCode\": \"\", \"dateCredited\": \"\", \"creditAmount\": \"\"}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "creditType", List.of("must not be empty"),
                                "externalId", List.of("must not be empty", "must be a valid UUID"),
                                "dateCredited", List.of("must not be empty"),
                                "creditAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"creditType\":\"StocksSource\", \"externalId\":\"some\", \"currencyCode\": \"AED\", " +
                                "\"dateCredited\": \"invalid\", \"creditAmount\": -1}",
                        Map.of(
                                "creditType", List.of("must have a valid among: [STOCKS_REVENUE, STOCKS_DIVIDEND, STOCKS_DELETE_TRANSACTION]"),
                                "externalId", List.of("must be a valid UUID"),
                                "dateCredited", List.of("must be a date in the past or in the present",
                                        "must be a valid YYYY-MM-DD date"),
                                "creditAmount", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"creditType\":\"STOCKS_REVENUE\", \"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
                                "\"currencyCode\": \"AED\", \"dateCredited\": \"3020-12-07\", \"creditAmount\": 1.5}",
                        Map.of(
                                "dateCredited", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> withdrawFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "dateWithdrawn", List.of("must not be empty"),
                                "withdrawAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"currencyCode\": \"\", \"dateWithdrawn\": \"\", \"withdrawAmount\": \"\"}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "dateWithdrawn", List.of("must not be empty"),
                                "withdrawAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"currencyCode\": \"AED\", \"dateWithdrawn\": \"invalid\", \"withdrawAmount\": -1}",
                        Map.of(
                                "dateWithdrawn", List.of("must be a date in the past or in the present",
                                        "must be a valid YYYY-MM-DD date"),
                                "withdrawAmount", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"currencyCode\": \"AED\", \"dateWithdrawn\": \"3020-12-07\", \"withdrawAmount\": 1}",
                        Map.of(
                                "dateWithdrawn", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> debitFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "debitType", List.of("must not be empty"),
                                "externalId", List.of("must not be empty"),
                                "dateDebited", List.of("must not be empty"),
                                "debitAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"debitType\":\"\", \"externalId\":\"\", \"currencyCode\": \"\", \"dateDebited\": \"\", \"debitAmount\": \"\"}",
                        Map.of(
                                "currencyCode", List.of("must not be empty"),
                                "debitType", List.of("must not be empty"),
                                "externalId", List.of("must not be empty", "must be a valid UUID"),
                                "dateDebited", List.of("must not be empty"),
                                "debitAmount", List.of("must not be null")
                        )),
                Arguments.of("{\"debitType\":\"StocksSource\", \"externalId\":\"some\", \"currencyCode\": \"AED\", " +
                                "\"dateDebited\": \"invalid\", \"debitAmount\": -1}",
                        Map.of(
                                "debitType", List.of("must have a valid among: [STOCKS_INVESTMENT, STOCKS_DELETE_TRANSACTION]"),
                                "externalId", List.of("must be a valid UUID"),
                                "dateDebited", List.of("must be a date in the past or in the present",
                                        "must be a valid YYYY-MM-DD date"),
                                "debitAmount", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"debitType\":\"STOCKS_INVESTMENT\", \"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
                                "\"currencyCode\": \"AED\", \"dateDebited\": \"3020-12-07\", \"debitAmount\": 1.5}",
                        Map.of(
                                "dateDebited", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> getMovementsReturnsNotFoundForInvalidPathParametersCases() {
        return Stream.of(
                Arguments.of("types=INVALID", Map.of("types", List.of("must have a valid among: [DEPOSIT, WITHDRAW, CREDIT, DEBIT]"))),
                Arguments.of("types=DEPOSIT,INVALID", Map.of("types", List.of("must have a valid among: [DEPOSIT, WITHDRAW, CREDIT, DEBIT]"))));
    }

    public static final String DEFAULT_ACCOUNT = "{" +
            "\"accountName\": \"DEFAULT\", " +
            "\"currency\": \"AED\"}";

    public static final String ACCOUNT_USD = "{" +
            "\"accountName\": \"DEFAULT_USD\", " +
            "\"currency\": \"USD\"}";

    public static final String DEFAULT_DEPOSIT = "{" +
            "\"currencyCode\": \"AED\", " +
            "\"dateDeposited\": \"2020-12-07\"," +
            "\"depositAmount\": 10.0}";

    public static final String DEPOSIT_USD = "{" +
            "\"currencyCode\": \"USD\", " +
            "\"dateDeposited\": \"2020-12-07\"," +
            "\"depositAmount\": 10.0}";

    public static final String DEPOSIT_5 = "{" +
            "\"currencyCode\": \"AED\", " +
            "\"dateDeposited\": \"2020-12-07\"," +
            "\"depositAmount\": 5.0}";

    public static final String DEFAULT_WITHDRAW = "{" +
            "\"currencyCode\": \"AED\", " +
            "\"dateWithdrawn\": \"2020-12-07\"," +
            "\"withdrawAmount\": 10.0}";

    public static final String WITHDRAW_USD = "{" +
            "\"currencyCode\": \"USD\", " +
            "\"dateWithdrawn\": \"2020-12-07\"," +
            "\"withdrawAmount\": 10.0}";

    public static final String WITHDRAW_DEC_10 = "{" +
            "\"currencyCode\": \"AED\", " +
            "\"dateWithdrawn\": \"2020-12-10\"," +
            "\"withdrawAmount\": 10.0}";

    public static final String DEFAULT_CREDIT = "{" +
            "\"creditType\":\"STOCKS_REVENUE\"," +
            " \"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
            "\"currencyCode\": \"AED\", " +
            "\"dateCredited\": \"2020-12-07\", " +
            "\"creditAmount\": 10.0}";

    public static final String CREDIT_USD = "{" +
            "\"creditType\":\"STOCKS_REVENUE\"," +
            " \"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
            "\"currencyCode\": \"USD\", " +
            "\"dateCredited\": \"2020-12-07\", " +
            "\"creditAmount\": 10.0}";

    public static final String CREDIT_DEC_9 = "{" +
            "\"creditType\":\"STOCKS_REVENUE\"," +
            " \"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
            "\"currencyCode\": \"AED\", " +
            "\"dateCredited\": \"2020-12-09\", " +
            "\"creditAmount\": 10.0}";

    public static final String DEFAULT_DEBIT = "{" +
            "\"debitType\":\"STOCKS_INVESTMENT\", " +
            "\"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
            "\"currencyCode\": \"AED\", " +
            "\"dateDebited\": \"2020-12-07\", " +
            "\"debitAmount\": 10.0}";

    public static final String DEBIT_USD = "{" +
            "\"debitType\":\"STOCKS_INVESTMENT\", " +
            "\"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
            "\"currencyCode\": \"USD\", " +
            "\"dateDebited\": \"2020-12-07\", " +
            "\"debitAmount\": 10.0}";

    public static final String DEBIT_DEC_8 = "{" +
            "\"debitType\":\"STOCKS_INVESTMENT\", " +
            "\"externalId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\", " +
            "\"currencyCode\": \"AED\", " +
            "\"dateDebited\": \"2020-12-08\", " +
            "\"debitAmount\": 10.0}";
}
