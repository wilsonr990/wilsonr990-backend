package com.rgapps.components.datahelper;

import com.rgapps.components.model.BusinessCardComponent;

public class ModelData {
    public static BusinessCardComponent defaultBusinessCardComponent = new BusinessCardComponent(
            "someUrlIageLogo",
            "somePersonalName",
            "someProfession",
            "someComponentName");
}
