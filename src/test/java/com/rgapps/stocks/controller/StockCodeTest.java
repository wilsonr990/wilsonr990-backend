package com.rgapps.stocks.controller;

import com.rgapps.stocks.IntegrationTest;
import com.rgapps.stocks.datahelper.DataHelper;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.values.Stock;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class StockCodeTest extends IntegrationTest {
    @Test
    public void stockValueSavedOnBuyCorrectly() {
        BuyStockTransaction response = buyStock(DataHelper.DEFAULT_BUY)
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        Stock registry = mongoTemplate.findById(response.getStock().getCode(), Stock.class).block();
        assertThat(registry).isEqualTo(response.getStock());
    }

    @Test
    public void stockValueRetrieveCodesCorrectly() {
        buyStock(DataHelper.DEFAULT_BUY);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_GHI);

        List<String> response = webTestClient.get()
                .uri("/api/stocks/code")
                .exchange()
                .expectStatus().isOk()
                .expectBody(DataHelper.StringList.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).containsAll(List.of("ABC", "DEF", "GHI"));
    }
}
