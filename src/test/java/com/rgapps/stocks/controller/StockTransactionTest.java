package com.rgapps.stocks.controller;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import com.rgapps.stocks.IntegrationTest;
import com.rgapps.stocks.controller.resource.BuyStockResource;
import com.rgapps.stocks.controller.resource.GetDividendResource;
import com.rgapps.stocks.controller.resource.SellStockResource;
import com.rgapps.stocks.controller.resource.StockTransactionView;
import com.rgapps.stocks.datahelper.DataHelper;
import com.rgapps.stocks.datahelper.ModelData;
import com.rgapps.stocks.datahelper.ObjectValidations;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockTransaction;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Disabled("Embedded mongo not working inside docker")
class StockTransactionTest extends IntegrationTest {
    @ParameterizedTest
    @MethodSource("com.rgapps.stocks.datahelper.DataHelper#buyStockValidationsBodyCases")
    public void buyStockFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void buyStockCorrectly() {
        LocalDateTime start = LocalDateTime.now();
        BuyStockTransaction response = buyStock(DataHelper.DEFAULT_BUY)
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateTransaction(response,
                StockTransaction.Type.BUY,
                StockTransaction.Status.SUCCESS,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
        assertThat(response).isNotNull();
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void buyStockFailsWhenCurrenciesDifferFromAccountCurrency() {
        ModelErrorResponse<BuyStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.BUY_ANOTHER_CURRENCY_USD)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseBuyStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");

        assertThat(response).isNotNull();
        BuyStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateBuyResource(erroredObject,
                "ABC",
                1,
                "USD",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void buyStockFailsWhenTransactionIsOlderThanLastBuyTransactionForGivenStock() {
        buyStock(DataHelper.DEFAULT_BUY);

        ModelErrorResponse<BuyStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.BUY_BEFORE)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseBuyStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Date Sold' (expected: after 2020-12-07, actual: 2020-12-06)");

        assertThat(response).isNotNull();
        BuyStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateBuyResource(erroredObject,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-06");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void buyStockFailsWhenTransactionIsOlderThanLastSellTransactionForGivenStock() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_NEXT_DAY);

        ModelErrorResponse<BuyStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseBuyStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Date Sold' (expected: after 2020-12-08, actual: 2020-12-07)");

        assertThat(response).isNotNull();
        BuyStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateBuyResource(erroredObject,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
    }

    @Test
    public void buyStockFailsWhenAccountHasNoMoneyToBuyStocks() {
        ModelErrorResponse<BuyStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.BUY_10000_STOCKS)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseBuyStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Money To Buy Stock' (expected: <= 100.0, actual: 100001.0)");

        assertThat(response).isNotNull();
        BuyStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateBuyResource(erroredObject,
                "ABC",
                10000,
                "AED",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void buyStockFailsWhenAccountSummaryFails() {
        WireMock.reset();
        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from GET http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/summary");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void buyStockFailsWhenAccountDebitFails() {
        WireMock.reset();
        enableAccountSummary();
        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void buyStockFailsIfNotAuthenticated() {
        webTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.stocks.datahelper.DataHelper#sellStockValidationsBodyCases")
    public void sellStockFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void sellStockCorrectly() {
        buyStock(DataHelper.BUY_TWO_SHARES);

        LocalDateTime start = LocalDateTime.now();
        SellStockTransaction response = sellStock(DataHelper.DEFAULT_SELL)
                .expectBody(SellStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransaction(response,
                StockTransaction.Type.SELL,
                StockTransaction.Status.SUCCESS,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void sellStockFailsWhenCurrenciesDiffer() {
        buyStock(DataHelper.DEFAULT_BUY);

        ModelErrorResponse<SellStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.SELL_ANOTHER_CURRENCY)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSellStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");

        assertThat(response).isNotNull();
        SellStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSellResource(erroredObject,
                "ABC",
                1,
                "USD",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void sellStockFailsWhenStockHasNotBeenBought() {
        ModelErrorResponse<SellStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SELL)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSellStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Number Of Shares' (expected: <= 0, actual: 1)");

        assertThat(response).isNotNull();
        SellStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSellResource(erroredObject,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void sellStockFailsWhenNumberOfStockSharesIsNotEnough() {
        buyStock(DataHelper.DEFAULT_BUY);

        ModelErrorResponse<SellStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.SELL_TWO_SHARES)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSellStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Number Of Shares' (expected: <= 1, actual: 2)");

        assertThat(response).isNotNull();
        SellStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSellResource(erroredObject,
                "ABC",
                2,
                "AED",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void sellStockFailsWhenTransactionIsOlderThanLastSellTransactionForGivenStock() {
        buyStock(DataHelper.DEFAULT_BUY);

        ModelErrorResponse<SellStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.SELL_BEFORE)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSellStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Date Sold' (expected: after 2020-12-07, actual: 2020-12-06)");

        assertThat(response).isNotNull();
        SellStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSellResource(erroredObject,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-06");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void sellStockFailsWhenTransactionIsOlderThanLastBuyTransactionForGivenStock() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_NEXT_DAY);

        ModelErrorResponse<SellStockResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SELL)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseSellStockResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Date Sold' (expected: after 2020-12-08, actual: 2020-12-07)");

        assertThat(response).isNotNull();
        SellStockResource erroredObject = response.getErroredObject();
        ObjectValidations.validateSellResource(erroredObject,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
    }

    @Test
    public void sellStockFailsWhenAccountSummaryFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        WireMock.reset();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SELL)
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from GET http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/summary");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void sellStockFailsWhenAccountCreditFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        WireMock.reset();
        enableAccountSummary();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SELL)
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void sellStockFailsIfNotAuthenticated() {
        webTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.stocks.datahelper.DataHelper#getDividendValidationsBodyCases")
    public void getDividendFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/getDividend?accountId=" + ModelData.defaultAccountId + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void getDividendCorrectly() {
        buyStock(DataHelper.BUY_TWO_SHARES);

        LocalDateTime start = LocalDateTime.now();
        GetDividendTransaction response = getDividend(DataHelper.DEFAULT_GET_DIVIDEND)
                .expectBody(GetDividendTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransaction(response,
                StockTransaction.Type.DIVIDEND,
                StockTransaction.Status.SUCCESS,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void getDividendFailsWhenCurrenciesDiffer() {
        buyStock(DataHelper.DEFAULT_BUY);

        ModelErrorResponse<GetDividendResource> response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/getDividend?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.GET_DIVIDEND_ANOTHER_CURRENCY)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseGetDividendResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");

        assertThat(response).isNotNull();
        GetDividendResource erroredObject = response.getErroredObject();
        ObjectValidations.validateGetDividendResource(erroredObject,
                "ABC",
                1,
                "USD",
                10.0,
                1.0,
                "2020-12-07");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void getDividendFailsWhenAccountSummaryFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        WireMock.reset();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/getDividend?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_GET_DIVIDEND)
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from GET http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/summary");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void getDividendFailsWhenAccountCreditFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        WireMock.reset();
        enableAccountSummary();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/getDividend?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_GET_DIVIDEND)
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void getDividendFailsIfNotAuthenticated() {
        webTestClient.post()
                .uri("/api/stocks/transactions/getDividend?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void deleteLastSavesAsDeletedForBuyTransaction() {
        LocalDateTime start = LocalDateTime.now();
        buyStock(DataHelper.DEFAULT_BUY);

        LocalDateTime update = LocalDateTime.now();
        BuyStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isOk()
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransactionUpdated(response,
                StockTransaction.Type.BUY,
                StockTransaction.Status.DELETED,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                update,
                update,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void deleteLastSavesAsDeleteForSellTransaction() {
        buyStock(DataHelper.DEFAULT_BUY);
        LocalDateTime start = LocalDateTime.now();
        sellStock(DataHelper.DEFAULT_SELL);
        LocalDateTime update = LocalDateTime.now();
        SellStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isOk()
                .expectBody(SellStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransactionUpdated(response,
                StockTransaction.Type.SELL,
                StockTransaction.Status.DELETED,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                update,
                update,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void deleteLastSavesAsDeleteForDividendTransaction() {
        buyStock(DataHelper.DEFAULT_BUY);
        LocalDateTime start = LocalDateTime.now();
        getDividend(DataHelper.DEFAULT_GET_DIVIDEND);
        LocalDateTime update = LocalDateTime.now();
        GetDividendTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isOk()
                .expectBody(GetDividendTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransactionUpdated(response,
                StockTransaction.Type.DIVIDEND,
                StockTransaction.Status.DELETED,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                update,
                update,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void deleteLastFailWithNotFoundErrorWhenTransactionNotFound() {
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response,
                HttpStatus.NOT_FOUND,
                "NotFoundException",
                "Not found 'Success Transaction' ({CreatedDate=oldest})");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(0);
    }

    @Test
    public void deleteLastFailsForBuyTransactionWhenAccountCreditFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        WireMock.reset();
        enableAccountSummary();
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(updatedTransaction.getStatus()).isEqualTo(SUCCESS);
    }

    @Test
    public void deleteLastFailsForSellTransactionWhenAccountDebitFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        sellStock(DataHelper.DEFAULT_SELL);
        WireMock.reset();
        enableAccountSummary();
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(updatedTransaction.getStatus()).isEqualTo(SUCCESS);
    }

    @Test
    public void deleteLastFailsForDividendTransactionWhenAccountDebitFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        getDividend(DataHelper.DEFAULT_GET_DIVIDEND);
        WireMock.reset();
        enableAccountSummary();
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(updatedTransaction.getStatus()).isEqualTo(SUCCESS);
    }

    @Test
    public void deleteLastFailsIfNotAuthenticated() {
        webTestClient.post()
                .uri("/api/stocks/transactions/last")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void deleteLastByStockSavesAsDeleteForBuyTransaction() {
        LocalDateTime start = LocalDateTime.now();
        buyStock(DataHelper.DEFAULT_BUY);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);

        LocalDateTime update = LocalDateTime.now();
        BuyStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransactionUpdated(response,
                StockTransaction.Type.BUY,
                StockTransaction.Status.DELETED,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                update,
                update,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void deleteLastByStockSavesAsDeleteForSellTransaction() {
        buyStock(DataHelper.DEFAULT_BUY);
        LocalDateTime start = LocalDateTime.now();
        sellStock(DataHelper.DEFAULT_SELL);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);
        LocalDateTime update = LocalDateTime.now();
        SellStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(SellStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransactionUpdated(response,
                StockTransaction.Type.SELL,
                StockTransaction.Status.DELETED,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                update,
                update,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(3);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void deleteLastByStockSavesAsDeleteForDividendTransaction() {
        buyStock(DataHelper.DEFAULT_BUY);
        LocalDateTime start = LocalDateTime.now();
        getDividend(DataHelper.DEFAULT_GET_DIVIDEND);
        LocalDateTime update = LocalDateTime.now();
        GetDividendTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(GetDividendTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        ObjectValidations.validateTransactionUpdated(response,
                StockTransaction.Type.DIVIDEND,
                StockTransaction.Status.DELETED,
                "ABC",
                1,
                "AED",
                10.0,
                1.0,
                "2020-12-07",
                start,
                update,
                update,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(2);
        StockTransaction registry = mongoTemplate.findById(response.getId(), StockTransaction.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void deleteLastByStockFailWithNotFoundErrorWhenTransactionNotFound() {
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);

        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isNotFound()
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response,
                HttpStatus.NOT_FOUND,
                "NotFoundException",
                "Not found 'Success Transaction' ({createdDate=oldest, stockCode=ABC})");

        assertThat(mongoTemplate.findAll(StockTransaction.class).collectList().block()).hasSize(1);
    }

    @Test
    public void deleteLastByStockFailsForBuyTransactionWhenAccountCreditFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);
        WireMock.reset();
        enableAccountSummary();
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(updatedTransaction.getStatus()).isEqualTo(SUCCESS);
    }

    @Test
    public void deleteLastByStockFailsForSellTransactionWhenAccountCreditFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        sellStock(DataHelper.DEFAULT_SELL);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);
        WireMock.reset();
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(updatedTransaction.getStatus()).isEqualTo(SUCCESS);
    }

    @Test
    public void deleteLastByStockFailsForDividendTransactionWhenAccountCreditFails() {
        buyStock(DataHelper.DEFAULT_BUY);
        getDividend(DataHelper.DEFAULT_GET_DIVIDEND);
        WireMock.reset();
        ApiErrorResponse response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isEqualTo(INTERNAL_SERVER_ERROR)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, INTERNAL_SERVER_ERROR,
                "ClientCallException",
                "404 Not Found from POST http://localhost:7777/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit");

//      TODO: undo database changes on failures. This should be fixed with transactional but it's not straightforward
//        check: https://stackoverflow.com/questions/56360094/calling-methods-in-two-different-reactivemongorepositorys-in-a-transaction-usin
//      assertThat(updatedTransaction.getStatus()).isEqualTo(SUCCESS);
    }

    @Test
    public void deleteLastByStockFailsIfNotAuthenticated() {
        webTestClient.post()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void getTransactionsGetOnlySuccessTransactions() {
        buyStock(DataHelper.DEFAULT_BUY);
        authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/buy?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.BUY_ANOTHER_CURRENCY_USD)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT);
        getDividend(DataHelper.DEFAULT_GET_DIVIDEND);
        sellStock(DataHelper.DEFAULT_SELL);
        authenticatedWebTestClient.post()
                .uri("/api/stocks/transactions/sell?accountId=" + ModelData.defaultAccountId)
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_SELL)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT);

        List<StockTransactionView> response = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(StockTransactionView.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).hasSize(3);
        ObjectValidations.validateTransactionView(response.get(0),
                StockTransaction.Type.SELL,
                "2020-12-07",
                "ABC",
                1,
                10.0,
                1.0,
                9.0,
                9,
                "AED");
        ObjectValidations.validateTransactionView(response.get(1),
                StockTransaction.Type.DIVIDEND,
                "2020-12-07",
                "ABC",
                1,
                10.0,
                1.0,
                11.0,
                11,
                "AED");
        ObjectValidations.validateTransactionView(response.get(2),
                StockTransaction.Type.BUY,
                "2020-12-07",
                "ABC",
                1,
                10.0,
                1.0,
                11.0,
                11,
                "AED");
    }

    @Test
    public void getTransactionsFailsIfNotAuthenticated() {
        webTestClient.post()
                .uri("/api/stocks/transactions")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_BUY)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }
}
