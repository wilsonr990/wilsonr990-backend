package com.rgapps.stocks;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.matching.RegexPattern;
import com.rgapps.commons.AuthenticatedUsers;
import com.rgapps.stocks.datahelper.ModelData;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.Stock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.PostConstruct;

import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@AutoConfigureWireMock(port = 7777)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTest {
    @Autowired
    protected ApplicationContext context;

    @Autowired
    protected ReactiveMongoTemplate mongoTemplate;

    @Autowired
    protected AuthenticatedUsers authenticatedUsers;

    @Autowired
    protected WebTestClient authenticatedWebTestClient;

    @Autowired
    protected WebTestClient webTestClient;

    @PostConstruct
    public void init() {
        authenticatedWebTestClient = authenticatedWebTestClient
                .mutate()
                .defaultHeader(HttpHeaders.AUTHORIZATION,
                        String.format("Bearer %s", authenticatedUsers.userNormalToken()))
                .build();
    }

    @BeforeEach
    public void enableMocks() {
        WireMock.reset();
        enableAccountDebit();
        enableAccountCredit();
        enableAccountSummary();
    }

    @BeforeEach
    public void clean() {
        mongoTemplate.findAllAndRemove(new Query(), BuyStockTransaction.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), SellStockTransaction.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), GetDividendTransaction.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), StockJournal.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), MonthlyProfitSummary.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), Currency.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), Stock.class).collectList().block();
    }

    protected void enableAccountSummary() {
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/accounts/%s/summary", ModelData.defaultAccountId)))
                .withHeader(HttpHeaders.AUTHORIZATION, new RegexPattern("Bearer .*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"accountId\":\"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa\"," +
                                "\"accountName\": \"Name\"," +
                                "\"currency\": \"AED\"," +
                                "\"amount\": \"100.0\"}")));
    }

    protected void enableAccountDebit() {
        WireMock.stubFor(WireMock.post(urlEqualTo(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccountId)))
                .withHeader(HttpHeaders.AUTHORIZATION, new RegexPattern("Bearer .*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("")));
    }

    protected void enableAccountCredit() {
        WireMock.stubFor(WireMock.post(urlEqualTo(String.format("/api/accounts/%s/movements/credit", ModelData.defaultAccountId)))
                .withHeader(HttpHeaders.AUTHORIZATION, new RegexPattern("Bearer .*"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("")));
    }

    protected WebTestClient.ResponseSpec buyStock(String body) {
        return authenticatedWebTestClient.post()
                .uri(String.format("/api/stocks/transactions/buy?accountId=%s", ModelData.defaultAccountId))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec sellStock(String body) {
        return authenticatedWebTestClient.post()
                .uri(String.format("/api/stocks/transactions/sell?accountId=%s", ModelData.defaultAccountId))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec getDividend(String body) {
        return authenticatedWebTestClient.post()
                .uri(String.format("/api/stocks/transactions/getDividend?accountId=%s", ModelData.defaultAccountId))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }
}
